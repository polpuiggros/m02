#  1 - Caracterització dels sistemes operatius, tipus i aplicacions 

Taula de continguts

[TOC]

## 1.1 - Els components físics i lògics d'un sistema informàtic. El programari base

### La Màgia

Estàs davant d'un ordinador, davant la pantalla, deixant passar el temps. De cop i volta prems el botó màgic i aquest s'engega, pren vida. Sents els ventiladors que comencen a girar amb el seu soroll de flux d'aire característic. Unes llums s'engeguen al frontal de la caixa. Tot indica que alguna cosa passarà

De cop i volta apareixen caràcters a la pantalla. Textos incomprensibles El maquinari està executant les seves instruccions bàsiques per reconèixer-se a si mateix. Més enllà, el desconegut. De cop i volta apareix un logo gràfic a la pantalla. És el sistema operatiu que pren el control de la màquina. Esperes Al cap d'una estona apareix una pantalla que demana identificar-te El sistema operatiu comença a interactuar amb tu.

Lentament i comprovant cada caràcter entres el teu usuari. Ara fas el mateix amb la clau. El teclat és la teva entrada de dades al sistema, et proporciona el control. Prems l'enter i un nou món s'obre davant els teus ulls Ara pots utilitzar amb desimboltura el ratolí, l'altre dispositiu d'entrada que té el sistema operatiu si és gràfic.

La màquina que fa una estona semblava un moble inert ha pres vida gràcies al sistema operatiu. Aquest sistema operatiu ens ha proporcionat la possibilitat de tenir una entrada a través del teclat i una sortida a través de la pantalla. La interacció amb la màquina és completa

El sistema operatiu s'ha encarregat de donar vida mitjançant els controladors al maquinari, des del disc dur d'on s'han llegit les dades, passant pel processador que les ha interpretat fins arribar a la pantalla i els dispositius d'entrada que ens han permès interactuar-hi

Un munt de dispositius electrònics han pres vida gràcies al sistema operatiu que els ha orquestrat per tal d'aconseguir un sistema capaç de processar i emmagatzemar les dades que nosaltres li proporcionem i mostrar-les per la pantalla.

La màgia ha estat possible gràcies al sistema operatiu. Ara un munt d'aplicacions es troben al nostre abast gràcies a la tasca del sistema operatiu, que ens farà d'intermediari amb el maquinari, que d'altra banda seria inert.

Arribats a aquest punt ens preguntem: quants sistemes operatius existiran?

Molts i diversos, però tots amb una finalitat: donar vida al maquinari i així permetre que nosaltres com a usuaris puguem executar aplicatius també diversos, des de processadors de textos a jocs hiperrealistes Al sistema operatiu tant li fa, només veurà que com a usuaris entrem dades, ja sigui amb el teclat o el ratolí, les processarà al processador i bolcarà els resultats per pantalla o els emmagatzemarà al disc. Tot amb una rapidesa i precisió dignes del millor mag.

Gràcies al sistema operatiu podrem configurar aquests dispositius físics: la velocitat del ratolí, la resolució de la pantalla, el partionat de les dades al disc dur, la direcció de xarxa de la connexió a Internet... un munt de coses ens deixarà configurar al nostre gust el sistema operatiu. Ell és qui ha detectat aquests dispositius Ells és qui en té l'absolut control i ell és qui ens deixarà configurar-los. Les aplicacions simplement en faran ús i gaudiran d'aquestes possibilitats per donar-nos com a usuaris l'opció de realitzar altres tasques específiques: editar un document, retocar una fotografia, proporcionar una plana web al món, ... les possibilitats són infinites!

Benvinguts/des a l'apassionant món dels sistemes operatius!

### Resumint la màgia

Un sistema informàtic es composa de  *maquinari*, *programari* i *humans* que hi interactuen.

Dit d'una altra manera, uns sistemes *físics* governats per un sistema *lògic*  que controla l'*usuari*.

- Maquinari
- Programari
  - Bàsic: Sistema Operatiu
   - d'Aplicació: Tasques determinades
- Humans:
  - Usuari
  - Personal informàtic
    - Direcció
    - Anàlisi
    - Programació
    - Explotació
    - Operadors

Tot aquest programari està escrit en *llenguatges* diversos. Hi haurà llenguatges que ens seran més comprensibles, els *llenguatges de programació* que seran traduïts a llenguatges incomprensibles per a nosaltres però molt fàcils d'interpretar pel maquinari, el *llenguatge màquina*.

Llenguatges:

- de programació
- màquina

### Una mirada als sistemes operatius

Existeixen molts i diversos sistemes operatius. Tots destinats a proporcionar-nos com a usuaris un entorn entre les aplicacions i el maquinari que governarà el propi sistema operatiu.

Els sistemes operatius poden ser molt enrevessats i proporcionar complexos sistemes gràfics, molt útils quan volem un entorn per a aplicacions gràfiques. Però tot al seu moment. No sempre ens caldrà un complex i costós entorn gràfic per a editar un document o per a servir planes web al món sencer. Aquest costós sistema gràfic només consumirà recursos innecessaris per a la funció que volem obtenir. Cal tenir clar doncs quina és la finalitat del sistema operatiu que farem servir sobre el maquinari.

- **Interfície de línia d'ordres**: Un sistema operatiu sense interfície gràfica que consumeixi tots aquests recursos serà ideal si el que volem és proporcionar serveis en xarxa. És per això que sistemes operatius com el Linux són àmpliament utilitzats com a servidors, ja sigui de fitxers, de planes web, de correu, etc... En teniu un exemple a: https://www.tutorialspoint.com/unix_terminal_online.php
- **Interfície gràfica**: Els sistemes operatius amb interfície gràfica són molt útils com a estacions de treball per a l'usuari que necessitarà executar aplicacions en les que introduirà dades i voldrà veure'n els resultats de manera gràfica. Un exemple accessible des del navegador: https://demo.os-js.org/

```markdown
# PROVEM-HO

Aneu al web de l'IsardVDI i creeu diferents sistemes operatius. En aquest cas seran virtuals, es a dir, un maquinari remot executarà el sistema operatiu que seleccionem que ha estat instal·lat prèviament i ens proporcionarà l'interfície a través d'un visor que també hi connectarà les nostres entrades, teclat i ratolí.
- Creeu un sistema operatiu Centos 7
- Creeu un sistema operatiu Debian 9
- Creeu un sistema operatiu Windows 7
Inicieu-los i conecteu-vos-hi. Interactueu amb el sistema i vegeu-ne les diferències entre un entorn de línia de comandes (CLI) i un entorn gràfic (GUI).
```

```markdown
# INTERACTUEM AMB EL SISTEMA OPERATIU

Realitzeu les següents operacions en línia de comandes (CLI) i mitjançant la interfície gràfica (GUI) fent ús de diferents sistemes operatius:
- Mostrar espai ocupat en disc
- Veure el consum de recursos de maquinari, com CPU i memòria
- Mostrar els dispositius d'emmagatzematge (discs) detectats al sistema
- Editar un fitxer de text de nom apartat1.md que contingui les vostres dades personals.

Reflexionem: Quin sistema és més àgil per a realitzar cadascuna les les operacions anteriors?
```

### Una mirada als llenguatges de programació

Tal com hem comentat existeix el *llenguatge màquina*. Aquest és el llenguatge bàsic que serà processat pel processador. En teniu un exemple d'aquest llenguatge anomenat *assembler* a: https://www.tutorialspoint.com/compile_assembly_online.php

Però per als usuaris ens és més fàcil utilitzar llenguatges de programació de més alt nivell. Un exemple és el llenguatge de programació *python*. En teniu un exemple a: https://www.tutorialspoint.com/execute_python_online.php. En aquest cas el senzill programa python serà convertit a l'assembler de l'exemple anterior i executat pel processador, obtenint el mateix resultat. La avantatge és que els llenguatges d'alt nivell com el python són llenguatges molt més comprensibles i pròxims als llenguatges que utilitzem els humans.

Hi ha molts més llenguatges de programació d'alt nivell, cadascun es troba més enfocat a algun tipus de finalitat:

- javascript: llenguatge de programació que interpreten els navegadors web.
  - https://playcode.io/online-javascript-editor
- c: llenguatge molt potent, tant que molts sistemes operatius estan programats en aquest llenguatge.
  - https://www.tutorialspoint.com/compile_c_online.php
- go: llenguatge molt nou enfocat a entorns web.
  - https://play.golang.org/
- bash: llenguatge de programació per a línia de comandes linux
  - https://www.jdoodle.com/test-bash-shell-script-online
- vbscript: llenguatge de programació per automatitzar tasques en sistemes Windows
  - https://www.onlinegdb.com/online_vb_compiler

## 1.2 - Codificació de la informació en diferents sistemes de representació

Dades:
- Numèriques
- Alfanumèriques
  ...
```
Mostrar exemples de dades: binari, variables numèriques (ALU) i strings
```
El **teorema fonamental de la numeració** (**TFN**) diu que el valor decimal d’una quantitat expressada en altres sistemes de numeració s’expressa segons el polinomi següent: 

 ![img](http://ioc.xtec.cat/materials/FP/Materials/2201_SMX/SMX_2201_M02/web/html/WebContent/u1/media/imge56d0e0660c2f27d1672cbfd3e8fc43d.png) 

 en què el símbol *b* representa la base i *x* són els dígits del nombre. 

Exemples de conversió a decimal

 **1)** Donat el número ![img](http://ioc.xtec.cat/materials/FP/Materials/2201_SMX/SMX_2201_M02/web/html/WebContent/u1/media/img222bb918194f3ca7c7206323d07bda16.png) , en calculem el valor decimal. Apliquem el TFN: 

 ![img](http://ioc.xtec.cat/materials/FP/Materials/2201_SMX/SMX_2201_M02/web/html/WebContent/u1/media/imgd2174b1eff8455d8752cec81baae7da4.png) 

 Recordeu que qualsevol nombre elevat a zero val 1. 

 **2)** Tenim la quantitat ![img](http://ioc.xtec.cat/materials/FP/Materials/2201_SMX/SMX_2201_M02/web/html/WebContent/u1/media/img03460de4096778aa8393718c4dec2fe0.png)  que s’expressa en base 4, aquesta base utilitza per a representar  quantitats els dígits 0, 1, 2 i 3. Quin serà el valor corresponent en el  sistema decimal? 

 ![img](http://ioc.xtec.cat/materials/FP/Materials/2201_SMX/SMX_2201_M02/web/html/WebContent/u1/media/img47cb16d70852b71db2c4398b90bf6300.png)

*Referència: http://ioc.xtec.cat/materials/FP/Materials/2201_SMX/SMX_2201_M02/web/html/WebContent/u1/a1/continguts.html

### Binari

Els sistemes digitals, com els ordinadors, processen la informació bàsica com a encès (1) o apagat (0). Aquests valors en realitat són voltatges de +5V o +3V i de 0V.

Anomenem bits a aquesta informació bàsica de 1 o 0.

L'agrupació de 8 bits s'anomena byte i té com a unitat el Byte (B) i cada posició prendrà un pes El pes no és res més que el valor en el nostra codi decimal que pren un bit segons la posició que ocupa, de menys a més començant sempre per la dreta. Aquests pesos seran potències de 2. De 2 perquè és binari i només pot prendre dos valors, 1 o 0. 

En canvi, el nostre codi decimal pot prendre valors de 0 a 9 i per tant si els agrupem, cada posició tindrà un pes potència de 10 (el codi decimal pot prendre 10 valors, de 0 a 9).

| Decimal | Binari | Hexadecimal | Octal |
| ------- | ------ | ----------- | ----- |
| 0       | 0000   | 0           | 0     |
| 1       | 0001   | 1           | 1     |
| 2       | 0010   | 2           | 2     |
| 3       | 0011   | 3           | 3     |
| 4       | 0100   | 4           | 4     |
| 5       | 0101   | 5           | 5     |
| 6       | 0110   | 6           | 6     |
| 7       | 0111   | 7           | 7     |
| 8       | 1000   | 8           | 10    |
| 9       | 1001   | 9           | 11    |
| 10      | 1010   | A           | 12    |
| 11      | 1011   | B           | 13    |
| 12      | 1100   | C           | 14    |
| 13      | 1101   | D           | 15    |
| 14      | 1110   | E           | 16    |
| 15      | 1111   | F           | 17    |

```
Anem a convertir el nombre 11001011 a Sistema decimal:
    PAS 1 - Numerem els bits de dreta a esquerra començant des del 0.
    PAS 2 - A cada bit li fem correspondre una potència de base 2 i exponent igual al nombre de bit.
    PAS 3 - Finalment es sumen totes les potències.

POTÈNCIES:	7	6	5	4	3	2	1	0
CODI:		1	1	0	0	1	0	1	1

1·2⁷ + 1·2⁶ + 0·2⁵ + 0·2⁴ + 1·2³ + 0·2² + 1·2¹ + 1·2⁰ = 203
```

```
Anem a convertir el nombre 45 a binari:
    PAS 1 - Dividim 45 entre 2 successivament, sense treure decimals, fins a obtenir un quocient igual a 1.
    PAS 2 - Llegim l'últim quocient i totes les restes en sentit contrari a com han anat apareixent.
    PAS 3 - En cas que ens demanin el resultat dins d'un byte omplim amb zeros per davant fins a completar els vuit bits.

  45  /  2
  05     22  /  2
   1)    02     11  /  2
          0)     1)    5  /  2
                       1)    2  /  2
                             0)    1)
    <<------------------------------
  Ara escrivim de dreta a esquerra el codi: 1 0 1 1 0 1
  (Si cal afegirem zeros a l'esquerra per a completar un byte=8bits)
```

Amb aquest joc podem practicar la conversió entre el codi binari i el decimal: https://games.penjee.com/binary-numbers-game/

```
LES XARXES
Les xarxes fan servir majoritàriament les direccions IPv4. Aquestes direccions IP es formen per quatre bytes separats per punts. Busqueu quina és la vostra direcció IP amb la comanda: ip address show
L'ordinador ens mostra aquesta adreça com a 4 decimals (4 bytes) separats per punts, però ell internament ho tractarà com 4 bytes. Convertiu ara aquests 4 bytes al seu equivalent que processarà l'ordinador en bits. Podeu comprovar si ho heu fet bé utilitzant la utilitat de la línia de comandes bc.

Si us heu fixat, tot i que els ordinadors i dispositius en xarxa tenen adreça IP, nosaltres accedir als serveis (p.ex. web) d'aquests dispositius utilitzant noms de domini (p.ex. linux.org). Això és perquè ens és encara més fàcil recordar noms que no pas IP i molt més que no pas el format binari que fan servir internament els equips.
Busqueu adreces IP de dominis i convertiu-los a binari com faria un ordinador. Per a traduir un nom de domini a la seva adreça IP utilitzeu la comanda: nslookup linux.org

En aquest procés de convertir de nom de domini a adreça IP hi intervenen els servidors de noms de domini (DNS: Domain Name Service) que, encara que sembli mentida, emmagatzemen les traduccions dels noms a les seves corresponents adreces IP.
```

```
LÍNIA DE COMANDES
Des del bash de linux tenim el programari bc que ens permetrà fer conversions entre bases. Executem bc i introduïm la base d'entrada de dades (p.ex: ibase=10) i la base de sortida (p.ex: obase=2). Ara quan introduïm un nombre decimal (base d'entrada 10) ens el convertirà a binari (base de sortida 2).
Per sortir escriurem 'quit'
```

*APUNT*: Aviat podríem veure ordinadors que no treballen amb la unitat bàsica binària de 1 i 0 sinó amb estats de les partícules. Són les anomenades computadores quàntiques: https://www.youtube.com/watch?v=ItZj60njqmA. Això farà que encara siguin molt més ràpides i consumeixin menys energia.

### Hexadecimal

El codi hexadecimal, com el seu nom ja ens indica, cada dígit pot prendre 16 valors diferents: 0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F.

Per tant, si els agrupem, cada posició prendrà un pes que serà potència de 16.

```markdown
# LES TARGES DE XARXA
En tota comunicació d'un equip amb un altre per la xarxa ja hem vist que s'identifiquen per adreces IP. Però aquesta informació cal que localment tingui també una adreça més física, associada a la tarja de xarxa. Aquestes adreces es mostren en format hexadecimal. Podeu obtenir l'adreça física (MAC) de la vostra tarja a l'apartat link/ether de la comanda: **ip address show**
Traduïu, com ho faria l'ordinador, aquesta adreça hexadecimal a binari.
```

```markdown
# CODIS DE COLORS HTML
Els codis de colors en en llenguatge de programació de planes web HTML també es poden expressar en codi hexadecimal: [https://www.w3schools.com/colors/colors_picker.asp](https://www.w3schools.com/colors/colors_picker.asp) 
```

En aquest web hi ha un joc de matar marcians que duen un codi hexadecimal. Per tal de matar-los s'ha d'introduir en binari el codi hexadecimal que ens indiquen: [http://flippybitandtheattackofthehexadecimalsfrombase16.com/](http://flippybitandtheattackofthehexadecimalsfrombase16.com/). És força complicat. L'objectiu és superar a la nena de 2 anys que surt al vídeo jugant-hi.

### Octal

En el codi octal cada dígit pot prendre valors entre el 0 i el 7, que fan un total de vuit valors diferents. La seva base serà per tant 8 i cada posició tindrà un pes potència de 8.

```markdown
# EL SISTEMA DE PERMISOS DE FITXERS A LINUX
Com aprofundirem més endavant, el sistema de permisos de Linux indica si un fitxer/directori es pot llegir, escriure o executar mitjançant el codi octal. Cada dígit octal indicarà en binari quins permisos es donen (1) o es treuen (0).

DÍGIT OCTAL		BINARI		PERMISOS
	0			0 0 0		cap
	1			0 0 1		execució
	2			0 1 0		escriptura
	3			0 1 1		escriptura i execució
	4			1 0 0		lectura
	5			1 0 1		lectura i execució
	6			1 1 0		...
	7			...
```

## 1.3 - Sistemes transaccionals

- Entrada
- Emmagatzematge
- Processament
- Sortida
```
Entrada per teclat, emmagatzematge en fitxer, processat amb llenguatge i sortida per pantalla
Entrada des del web, emmagatzematge en full de càlcul/bbdd, processat amb llenguatge i sortida per fitxer.
```
### Entrada des d'API

L'entrada de dades per teclat és molt avorrida, especialment per a fer proves. Avui en dia hi ha fonts de dades de tot tipus que se'ns proporcionen mitjançant una API (Application Program Interface). Una api no és més que una URL (direcció web) que ens retorna un seguit de dades, molt habitualment en format JSON. Vegem-ne uns exemples a:

- https://jsonplaceholder.typicode.com/posts
- https://jsonplaceholder.typicode.com/albums

Nosaltres podem utilitzar aquestes fonts de dades com a entrada per a posteriorment emmagatzemar-les, processar-les i mostrar-les d'alguna manera. Farem un exemple que agafi dades en temps reals de l'ús de bicing a la ciutat de Barcelona https://api.bsmsa.eu/ext/api/bsm/gbfs/v2/en/station_status . 

Les dades tindran el format JSON que podeu veure si obriu l'adreça anterior amb un navegador:

```json
{"last_updated":1570435095,"ttl":30,"data":{"stations":[{"station_id":1,"num_bikes_available":2,"num_bikes_available_types":{"mechanical":2,"ebike":0},"num_docks_available":26,"is_installed":1,"is_renting":1,"is_returning":1,"last_reported":1570435001,"is_charging_station":true,"status":"IN_SERVICE"},{"station_id":2,"num_bikes_available":1,"num_bikes_available_types":{"mechanical":1,"ebike":0},"num_docks_available":26,"is_installed":1,"is_renting":1,"is_returning":1,"last_reported":1570434846,"is_charging_station":true,"status":"IN_SERVICE"},{"station_id":3,"num_bikes_available":3,"num_bikes_available_types":{"mechanical":3,"ebike":0},"num_docks_available":24,"is_installed":1,"is_renting":1,"is_returning":1,"last_reported":1570434985,"is_charging_station":true,"status":"IN_SERVICE"},{"station_id":4,"num_bike ...
```

En bash del sistema operatiu Linux tenim utilitats com wget o curl que ens permeten accedir des del CLI URL web. Si executeu ``curl https://api.bsmsa.eu/ext/api/bsm/gbfs/v2/en/station_status`` obtindreu el mateix resultat. Aquesta utilitat s'ha connectat a l'adreça (fent ús de la tarja de xarxa que ens proporciona el sistema operatiu) i ens ha bolcat el resultat a la tarja gràfica (que també ens proporciona el sistema operatiu).

Podríeu també provar a registrar-vos a https://openweathermap.org i mirar d'accedir a dades de previsió meteorològiques (forecast) per a la vostra població. Us deixo aquí el repte per a qui ho vulgui provar. Seguim amb les dades d'ús de bicing.

Nosaltres farem ús del llenguatge de programació python per a automatitzar aquest procés d'entrada de dades des del web de bicing, desar-les en disc i produir una gràfica de sortida. El programa el teniu al fitxer 4-bikesInUse_sortida.py.

### Conversió i Emmagatzematge

Depenent d'on vulguem emmagatzemar aquestes dades (que fins ara només es bolquen per pantalla) haurem de convertir-les d'una manera o altra.

Una manera d'emmagatzemar la sortida de la comanda curl anterior en un fitxer en comptes de bolcar-la per la pantalla és precisament redirigir-ne la sortida (fer un 'pipe') a un fitxer amb el redirector **>**:

```bash
curl https://api.bsmsa.eu/ext/api/bsm/gbfs/v2/en/station_status > data
```

Ara tindrem les dades anteriors que es mostraven per pantalla al fitxer **data**. Podem veure els fitxers del directori (on hi haurà el fitxer data) amb la comanda **ls**. I podem veure el contingut del fitxer amb la comanda **cat data**.

Si torneu a executar el curl anterior i torneu a mirar el fitxer amb cat veureu que s'ha sobreescrit el contingut i ara només hi ha les noves dades agafades del web, però no les anteriors. Per a afegir, en comptes de sobreescriure, el redirector ha de ser doble **>>**:

```bash
curl https://api.bsmsa.eu/ext/api/bsm/gbfs/v2/en/station_status >> data
```

Si ara mireu el contingut del fitxer amb **cat data** veureu que s'han afegit les noves dades al final.

El que podríem fer és agafar les dades en intervals de 10 segons durant un cert temps, per exemple un minut. Fer-ho manualment seria molest, per això farem un senzill script de python. Editarem un fitxer amb l'editor **vi** que trobem en tots els sistemes operatius basats en Linux, inclús els més senzills que podem trobar en un dispositiu *embedded* com pot ser un router de xarxa.

```bash
vi 1-bikesInUse_entrada.py
```

Hi ha dos modes a l'editor. D'entrada estem en mode comandes. La comanda per inserir dades en el fitxer és **i**. Ara podrem escriure dades. Una vegada acabem hem de tornar al mode comandes amb la tecla **ESC**. I la comanda per sortir guardant serà **:wq**. Si volguéssim sortir sense desar la comanda seria **:q!**. Fixeu-vos que tot són inicials d'accions en anglès: **i**nsertar, **w**rite, **q**uit i el signe d'exclamació per forçar la sortida encara que hi hagi canvis.

Teniu un *cheat sheet* aquí: http://www.lagmonster.org/docs/vi.html

Al fitxer hi escriurem les següents comandes:

```python
import requests
dataapi=requests.get("https://api.bsmsa.eu/ext/api/bsm/gbfs/v2/en/station_status").json()
print(dataapi)

```

Una vegada ho hagueu introduït al fitxer sortiu amb **ESC** i la comanda **:wq**. Podreu veure el contingut del fitxer fent **cat 1-bikesInUse_entrada.py**.

L'explicació d'aquest senzill programa python és que agafarà les dades de la URL de la API, les desarà en memòria (variable dataapi) i finalment les mostrarà per pantalla.

Ara ja podrem executar el fitxer amb ```python3 1-bikesInUse_entrada.py```

### Processat

Per tal de processar les dades no sempre serà fàcil. Cal poder accedir a les dades que tenim .

Podríem directament passar a la sortida (una gràfica per exemple) però també ens podria interessar processar aquestes dades emmagatzemades per tal d'obtenir per exemple la temperatura i pressions mitges per a les següents hores.

Fer la mitja és fàcil. Si tenim una llista de 6 intervals només caldrà sumar els valors i dividir-los per 6. D'aquesta manera podem obtenir els valors mitjos de temperatura i de pressió per a les properes hores

### Sortida

Amb aquestes dades podem passar a fer una gràfica que ens les mostri.

### El programa d'aplicatiu

Per a aquest exemple teniu el programa comentat al fitxer bikesInUse.py que podem executar des de línia de comandes amb l'intèrpret python d'aquesta manera ```python bikesInUse.py```. El programa requereix d'una interfície gràfica (GUI) en la fase final per tal de mostrar la sortida en una gràfica.

El programa també desarà les dades que ha retornat el web de bicing en un fitxer en format CSV anomenat 	databicing.csv. Proveu d'obrir-lo amb el full de càlcul del libreoffice i veureu que és un format simple d'emmagatzematge de dades.

Cal recordar que aquest programa d'aplicatiu farà ús de moltes funcions que proveeix el sistema operatiu. 

- En l'entrada de dades s'haurà de comunicar amb protocols i a través de la tarja de xarxa amb el web on resideix la API amb les dades. Aquestes dades s'hauran d'emmagatzemar a la memòria volàtil (RAM) que també gestiona el sistema operatiu.
- Per emmagatzemar-ho al disc en un fitxer, també caldrà que el sistema operatiu se'n faci carreg de desar-lo i recuperar-lo d'on l'hagi emmagatzemat posteriorment.
- La sortida gràfica serà generada a la tarja gràfica del nostre ordinador, que el sistema operatiu mitjançant els drivers s'encarregarà d'omplir punt a punt i color a color per tal que la tarja gràfica ho representi a la pantalla.

Tal com veieu aquest programa és un aplicatiu, senzill però aplicatiu, que no podria funcionar si no s'executés en un entorn de sistema operatiu que li proporciona accés d'entrada i sortida.

### 1.3.1 Tipus de sistemes d'informació

El sistema d'informació en una empresa serà habitualment una base de dades. Una base de dades és un seguit de taules que guarden relacions entre elles. Teniu exemples de bases de dades aquí: https://blog.capterra.com/free-database-software/. Per tant, habitualment l'emmagatzematge de les dades es farà en bases de dades.

Existeixen diferents tipus de bases de dades, però entre les més utilitzades trobem les tradicionals que són relacionals que basen les seves transaccions en el llenguatge SQL i les més modernes bases de dades documentals, que generalment s'anomenen noSQL perque precisament fan servir altres llenguatges per a fer les transaccions.

### 1.3.2 Transaccions

Normalment els usuaris faran **transaccions** sobre aquesta base de dades. Una transacció serà una o més accions que inseriran, modificaran o eliminaran dades en aquestes taules.

Tota transacció ha de complir quatre propietats conegudes amb el nom d’*ACID*: 

1.  Atomicitat (*atomicity*): és  la propietat que assegura que l’operació s’ha fet amb èxit o no i, per  tant, davant d’un problema en el sistema no pot quedar a mitges.
2.  Consistència (*consistency*): és la propietat que assegura que només es comença el procés que es pot acabar.
3.  Aïllament (*isolation*): és la propietat que assegura que una operació no en pot afectar d’altres.
4.  Permanència (*durability*):  és la propietat que assegura que una vegada executada l’operació,  aquesta persistirà i no es podrà desfer encara que falli el sistema.



## 1.4 - Concepte, elements i estructura d'un sistema operatiu

Com ja hem anat veient, un sistema operatiu és l'intermediari entre el maquinari i els aplicatius. Per tant hem de veure els sistemes operatius com els encarregats de controlar els dispositius de maquinari (processador, memòria, discs, teclats, gràfiques, xarxa) i proporcionar als aplicatius un entorn més estàndard per a realitzar funcions específiques fent ús d'aquests recursos que ens proporciona el sistema operatiu.

```markdown
# NAVEGAR PER INTERNET
Quins elements proporcionarà el sistema operatiu a l'aplicatiu 'navegador' (browser) per tal que es pugui executar i connectar-se a webs?
- Cal que hagi proporcionat un entorn gràfic (GUI) amb un gestor de finestres on el nostre navegador s'obrirà per a mostrar-nos menús, botons i una àrea d'informació. Per tant és el sistema operatiu qui gestionarà la tarja gràfica mitjançant els controladors (drivers) per a la tarja gràfica que tinguem.
- En introduir paraules de en un buscador, el sistema operatiu serà l'encarregat de rebre aquestes tecles que premem i proporcionar-les a l'aplicació navegador, que serà qui ens les anirà mostrant. També existeix un controlador (driver) al sistema operatiu encarregat d'interpretar els senyals que arriben del teclat.
- Per enviar i rebre dades cap a la xarxa l'aplicatiu navegador passarà aquestes dades al sistema operatiu, concretament al protocol TCP/IP, que s'encarregarà de formatar-les, trossejar-les i afegir-li unes capçaleres (com si fos una carta) per tal que puguin ser enviades per la tarja de xarxa. Aquesta tarja de xarxa també tindrà un controlador del sistema operatiu que s'encarregarà de transmetre i rebre aquestes dades per la xarxa entre nosaltres i el servidor remot.

Si us fixeu, l'aplicació ens proporciona una funcionalitat, navegar per Internet, però és el sistema operatiu l'encarregat de proporcionar-li a aquesta aplicació un entorn complert i funcional per tal de processar i comunicar-se, ja sigui per xarxa, per pantalla o per teclat.
```

### 1.4.2 Elements d'un sistema operatiu

**Nucli** (kernel)

Actua directament sobre el maquinari. És el que anomenem funcions de baix nivell ja que ha de comunicar-se amb els xips que incorpora aquest maquinari. Una de les principals funcions que farà es coordinar l'ús del processador, que també és el nucli del maquinari. Aquest processador ha de realitzar molts processos alhora i el kernel tindrà un **planificador** que prioritzarà aquests processos assignant-los un temps determinat a cadascun. Així evitarem que un procés d'una aplicació pugui consumir eternament el processador que necessiten altres aplicacions.


```markdown
- cat /proc/cpuinfo: Veurem informació del model i característiques de CPU. Podem analitzar a partir del web de Intel (ark.intel.com) l'arquitectura de nuclis (cores) i fils (threads) de diferents màquines. Amb l'utilitat htop es pot veure ràpidament quants fils en total permet la màquina.
- lscpu: és una altra instrucció que mostra més o menys el mateix.

A Windows podem extreure informació amb l'ordre: wmic cpu get caption, deviceid, name, numberofcores, maxclockspeed, status
o bé també gràficament a l'administrador de tasques (CTRL+ALT+SUPR) dins la pestanya de rendiment.
```

```markdown
- lspci: ens permet llistar els dispositius físics en bus PCI que ha detectat. Dóna informació de fabricant/model.
- lsmod: ens dóna informació de quins mòduls (controladors/drivers) es fan servir per a cada dispositiu detectat.

A Windows de manera gràfica podem clicar a Gestionar amb el botói dret sobre El meu ordinador i buscar el Gestor de dispositius. Per cada categoria podem desplegar i fent doble click sobre el dispositiu extreure'n el controlador (driver) i la versió.
```

**Administrador de memòria**

Cal que un mòdul del sistema operatiu s'encarregui de gestionar la memòria volàtil (RAM) del nostre ordinador. Això és del tot necessari ja que cada procés i aplicatiu que iniciem voldrà tenir una àrea de memòria per a emmagatzemar-hi coses.

En principi tot procés que iniciem creurà que pot utilitzar tota la memòria disponible en aquell moment per a ell. El problema evident és que no serà l'únic procés que iniciem, per tant, per cada aplicatiu que iniciem la memòria disponible es reduirà, podent arribar a un moment en el que el sistema no respongui. És per això que cal que les aplicacions que fem servir o programem facin un bon ús del recurs de memòria.

```markdown
# LA MEMÒRIA
Podem veure amb diferents comandes com s'està utilitzant la memòria del nostre sistema. En linux tot el referent a gestió del sistema operatiu o podem trobar dins el directory /proc. Per exemple podem mirar al fitxer meminfo amb la comanda: **cat /proc/meminfo**. Veurem que hi ha molts paràmetres que té en compte el sistema operatiu per a gestionar la memòria.
Altres comandes per a veure'n l'ús son:
- **free -m**
- **top**
Veureu que en la memòria hi sol aparèixer la memòria swap. Aquesta memòria en realitat és un espai físic en disc dur i, per tant, molt lent d'accedir-hi. Aquest espai en disc és on el sistema operatiu es reserva per a moure-hi dades d'aplicacions emmagatzemades en RAM quan aquestes aplicacions ja no s'usen tant (per exemple tenim ja 04 obert de fa estona un processador de textos però estem navegant per internet i no queda gaire memòria lliure).
És també important tenir en compte que en cas que el sistema operatiu es quedi sense memòria lliure i la swap també es vagi omplint, pot matar algun procés que consideri poc important segons la seva prioritat.
```

**Sistema d'entrada i sortida** (I/O)

El sistema operatiu ha de proporcionar les dades de manera el més independent possible del dispositu on s'han d'enviar o s'han de rebre. És a dir, no sempre tindrem disponible l'accés a guardar dades al disc si un altre aplicatiu n'està fent ús en aquell precís moment. Per tant el sistema operatiu crearà cues on els aplicatius deixaran immediatament les dades i el sistema operatiu en gestionarà quan pugui l'operació al dispositiu físic corresponent.

De vegades el sistema operatiu utilitzarà una monitorització dels dispositius per consultes periòdiques (polling) per tal de saber si ja estan disponibles. Altres dispositius (més habituals) permetran al sistema operatiu interrogar-los interrompent el que estiguin fent mitjançant interrupcions.

**Administrador d'arxius**

Cal que el sistema operatiu gestioni l'estructuració de les dades emmagatzemades per diferents usuaris i aplicatius. Aquesta estructura es basarà en directoris i fitxers principalment, però també pot encriptar les dades emmagatzemades i/o establir un sistema de permisos d'accés. El sistema de permisos d'un sistema operatiu és una part fonamental ja que permet entre altres coses decidir quins usuaris i aplicacions poden accedir a llegir, escriure o executar certs fitxers o directoris.

```markdown
# ESTRUCTURA D'ARXIUS I DIRECTORIS
Tindrem una estructura de directoris que seran les carpetes on hi haurà més directoris i/o fitxers.
L'arrel o punt principal d'aquesta estructura que és jeràrquica és a /. Per tant si fem un ls / veurem els directoris que pengen del punt arrel del sistema de fitxers.
Per a veure el directori on ens trobem podem usar la comanda pwd i per a canviar de directori la comanda cd.
Per crear un directori usarem la comanda mkdir i per a veure l'estructura de directoris en el seu format jeràrquic podem usar la comanda tree:

darta@mini:/$ tree -L 1
.
├── bin
├── boot
├── cdrom
├── dev
├── etc
├── home
├── initrd.img -> boot/initrd.img-4.13.0-45-generic
├── lib
├── lib64
├── lost+found
├── media
├── mnt
├── opt
├── proc
├── root
├── run
├── sbin
├── snap
├── srv
├── sys
├── tmp
├── usr
├── var
└── vmlinuz -> boot/vmlinuz-4.13.0-45-generic

Aquí veiem l'estructura típica d'un sistema linux, on les carpetes dels usuaris són al directori home:
darta@mini:/$ tree -L 1 /home
/home
├── darta
└── jvinolas
```

```markdown
# PERMISOS D'ARXIUS I DIRECTORIS
Podem veure els permisos i propietaris d'un arxiu o directori amb la comanda ls -lisa.

jvinolas@samba ~$ ls -lisa
total 162628
16778303      4 drwx------ 34 jvinolas inf                 4096 11 jul 10:47 .
15728652      4 drwxr-x--x 37 jordinas Administrators      4096 26 abr 10:34 ..
16778304     16 -rw-------  1 jvinolas inf                13331  9 jul 08:57 .bash_history
16908864      4 drwx------ 14 jvinolas inf                 4096 26 abr 11:22 .cache
16778431      4 drwxr-xr-x 30 jvinolas inf                 4096 11 gen 16:28 .config
16779156      4 -rw-r--r--  1 jvinolas inf                 1788 20 gen  2016 consola.vv
16912376      4 drwx------  3 jvinolas inf                 4096 12 feb  2014 .dbus
16779163      4 -rw-r-----  1 jvinolas inf                 1079  5 set  2014 debug.log
16778413      4 drwxr-xr-x  2 jvinolas inf                 4096  4 mai  2017 Descargas
16908712      4 drwxr-xr-x  2 jvinolas inf                 4096  4 mai  2017 Documentos
16908566      4 drwx------  2 jvinolas inf                 4096  6 mai  2014 Downloads

Si us fixeu en la tercera columna hi ha l'identificador de si es tracta o no d'un directori amb la lletra d.
A continuació en aquesta tercera columna hi ha repetits els permisos del fitxer tres vegades:
- r: read
- w: write
- x: execute
I fan referència als permisos de:
1) usuari
2) grup
3) altres
De manera que els permisos: -rw-r----- ens indiquen que l'usuari té permisos de lectura i escriptura sobre aquest fitxer, el grup només de lectura i la resta d'usuaris i grups no tenen cap permís sobre aquest fitxer.
L'usuari al que fa referència es troba a la quarta columna: jvinolas. I el grup al costat: inf
Per a modificar els permisos podem utilitzar la comanda chmod.
Per a modificar els propietaris podem utilitzar la comanda chown.
Feu un man chown i un man chmod per veure com fer-ne ús i proveu de canviar-ne usuaris, grups i permisos.
Per afegir usuaris al sistema ho farem amb adduser i per a grups groupadd.
```

### 1.4.3 Estructura d'un sistema operatiu

**Monolítica:** No té una estructuració o és molt mínima. Es tracta d'un seguit de funcions/procediments que es poden fer crides entre ells.

 **Jeràrquica o en capes:** Quan van anar creixent les necessitats es van començar a crear capes, cadascuna de les quals realitza funcions concretes.
 - Planificació del processador
 - Gestió de memòria
 - Gestió d'entrada/sortida
 - Sistema de fitxers
 - Programes d'usuari

 **De màquina virtual:** En realitat són sistemes operatius reals que funcionen sobre un maquinari simulat (el maquinari és programari que es comporta com ho faria el real)

 **Client/servidor:**: Es tracta de tenir un nucli el més petit possible i separat de la capa d'aplicacions. En aquests sistemes les aplicacions són clients que fan crides a processos del nucli.

 **Orientada a l’objecte:** És una definició de com s'ha programat el sistema operatiu. En aquests sistemes de programació orientada a l'objecte aquest és un element que té propietats i accions determinades.

 **Multiprocessador:** Aquests sistemes operatius són capaços d'executar diferents processos simultàniament. Depenent de com sigui l'arquitectura podem trobar sistemes:
 - Memòria compartida: Els processadors veuen tota la memòria del sistema i hi poden accedir. És l'arquitectura que trobem habitualment.
 - Memòria distribuïda: Cada processador és autònom amb la seva memòria i es comuniquen entre si mitjançant sistemes de missatges. Aquests sistemes són els usats en grans supercomputadors (que en realitat són molts computadors interconnectats)


```markdown
# LOCALITZEM EL KERNEL DEL SISTEMA OPERATIU
Habitualment en Linux trobarem els fitxers del nucli (kernel) dins la carpeta /boot. Si mireu dins amb ls veureu dos fitxers probablement:
- initrd
- vmlinuz
En arrencar l'ordinador el BOOT LOADER (gestor d'arrencada) carregarà en memòria els contingut dels dos fitxers.
El kernel llavors rebrà l'adreça de memòria on s'ha carregat l'initrd. Simplement l'interpretarà com si fos un sistema de fitxers en RAM. L'initrd contindrà un senzill programa anomenat init que detectarà el maquinari i carregarà els mòduls necessaris per a accedir-hi. 
Una vegada carregats els mòduls el kernel de l'vmlinuz continuarà carregant la resta del sistema de fitxers i arrencant el serveis necessaris per a funcionar.

Podem veure els mòduls de kernel carregats amb **lsmod**. Si pareu atenció hi veureu mòduls per a la tarja gràfica, àudio, bluetooth, xarxa, ...

Als moderns sistemes Linux podem veure el llistat de serveis que s'han iniciat per ordre amb la comanda **systemctl list-dependencies**. També podem veure tots els serveis, amb quins estan iniciats i quins no amb **systemctl list-unit-files**. Podem veure l'estat concret del servei amb **systemctl status <nom del servei>**.

És important diferenciar els mòduls, que permetran al sistema operatiu de gestionar els recursos de maquinari, dels serveis, que faran ús d'aquestes interfícies durant la seva execució.
```

## 1.5 - Funcions del sistema operatiu. Recursos

### 1.5.1 Objectius dels sistemes operatius

- Incrementar la productivitat dels usuaris (facilitant-ne l’ús).
- Proporcionar un entorn còmode i l’abstracció del maquinari a l’usuari.
- Optimitzar la utilització dels components o dels recursos del maquinari.
- Gestionar els recursos del maquinari i del programari.
- Decidir qui, quan, com i durant quant de temps s’utilitza un recurs.
- Resoldre conflictes entre peticions **concurrents** de manera que es preservi la integritat del sistema.
- Maximitzar el rendiment del sistema informàtic.
- Permetre la concurrència de processos.
- Possibilitar l’execució de qualsevol  procés en el moment en què se sol·liciti sempre que hi hagi prou  recursos lliures per a fer-ho.
- Ser eficient quant a reduir el temps  que ocupa cada treball, el temps que no s’utilitza la CPU, el temps de  resposta en sistemes multi-accés i el termini entre dos assignacions de  CPU a un mateix programa.
- Ser eficient quant a augmentar la  utilització de recursos en general, com ara la memòria, els  processadors, els dispositius d’E/S, etc.
- Ser fiable, és a dir, un sistema operatiu no ha de tenir errors i ha de preveure totes les possibles situacions.
- Tenir una grandària petita.
- Possibilitar i facilitar tant com es pugui el diàleg entre el maquinari i l’usuari.
- Permetre compartir entre diversos usuaris els recursos de maquinari que té un ordinador.
- Permetre als usuaris compartir dades entre ells, si escau.
- Facilitar les operacions d’E/S dels diferents dispositius connectats a un ordinador.

```
Si arrenquem tantes aplicacions/processos que el sistema operatiu no pot gestionar-les totes amb els recursos físics de què disposa aplicarà polítiques de prioritat, 'matant' els processos que consideri menys prioritaris.

Al Linux l'encarregat de prendre la decisió de matar un procés quan es queda sense memòria és l'**OOM Killer**. El procés que decidirà matar primer serà molt possiblement el que estigui consumint més memòria. 

Si arrenquem per exemple el navegador Firefox podem veure'n el seu número de procés amb la comanda **ps auxf | grep firefox**. El primer número després de l'usuari és el procés. Podem veure la puntuació que té a l'OOM Killer el Firefox que estem executant si mirem el contingut del fitxer. Exemple:

darta@mini:~$ ps auxf | grep firefox
darta    15463  0.0  0.0  15752  1024 pts/10   S+   22:36   0:00  |       \_ grep --color=auto firefox
darta    13280  9.5 16.5 2786952 644324 ?      Sl   19:34  17:26  \_ /usr/lib/firefox-esr/firefox-esr /home/darta/m02/uf1/uf1-caract_so_tipus_aplicacions.md
   
darta@mini:~$ cat /proc/13280/oom_score
83

Els possibles valors de puntuació poden anar de 0 (no consumeix memòria) a 1000 (consumeix tota la memòria).
```

### 1.5.2 Funcions dels sistemes operatius

**Faciliten la constitució d’una màquina virtual o estesa**.  El sistema operatiu posa al servei de l’usuari una màquina virtual que  té unes característiques que són diferents (i més fàcils d’abordar) que  les de la màquina real subjacent. Algunes àrees en les quals és freqüent  que la màquina virtual sigui diferent de la màquina real que la suporta  són:

1.  **Entrada/sortida (E/S**).  La capacitat d’E/S d’un maquinari bàsic pot ser que sigui extremament  complexa i que requereixi programes sofisticats per a utilitzar-lo. Un  sistema operatiu evita a l’usuari el problema d’haver de comprendre el  funcionament d’aquest maquinari i posa al seu abast una màquina virtual  més senzilla d’usar.
2.  **Memòria**. Molts  sistemes operatius presenten la imatge d’una màquina virtual en què la  memòria difereix en grandària de la de la màquina real subjacent. Així,  per exemple, un sistema operatiu pot utilitzar memòria secundària  (discos magnètics, etc.) per a crear la il·lusió d’una memòria principal  molt més extensa que la que es té en la realitat. Alternativament, pot  repartir la memòria principal entre diversos usuaris, de manera que  cadascun d’ells “vegi” una màquina virtual en què la memòria sigui més  petita que la de la màquina real.
3.  **Sistema de fitxers**.  La majoria de les màquines virtuals inclouen un sistema de fitxers per a  l’emmagatzematge a llarg termini tant de programes com de dades. El  sistema de fitxers es basa en la capacitat d’emmagatzematge sobre cinta o  disc de la màquina real. El sistema operatiu, no obstant això, permet a  l’usuari accedir a la informació emmagatzemada a partir de noms  simbòlics en lloc de fer-ho a partir de la seva posició física en el  mitjà d’emmagatzematge.
4.  **Protecció i tractament d’errors**.  Des del moment en què la majoria dels ordinadors són compartits per un  nombre determinat d’usuaris, és essencial que cadascun d’ells estigui  protegit dels efectes dels errors o de la mala fe dels altres. Els  ordenadors varien considerablement pel que fa al grau de protecció que  proporciona el seu **maquinari bàsic**; la missió del  sistema operatiu és constituir una màquina virtual en què cap usuari no  pugui afectar d’una manera negativa el treball dels altres.
5.  **Interacció a nivell de programa**.  Una màquina virtual pot possibilitar la interacció entre els diferents  programes dels usuaris de manera que, per exemple, la sortida d’un  d’ells s’utilitzi com a entrada d’un altre. La naturalesa concreta d’una  màquina virtual dependrà de l’aplicació particular a què es destini.  Així, per exemple, las característiques d’una màquina virtual que  controli un sistema de **temps real** serà diferent de les d’una màquina virtual que s’utilitzi per al desenvolupament de programes.

**Utilització compartida de recursos**.  Un sistema operatiu ha d’aconseguir que es comparteixin els recursos  d’un ordinador entre un cert nombre d’usuaris que treballen de manera  simultània. La finalitat és incrementar la disponibilitat de l’ordinador  respecte als usuaris i, alhora, maximitzar la utilització dels recursos  com processadors, memòries i dispositius d’E/S. La importància de la  utilització eficient d’aquests recursos influeix en el cost  d’utilització del sistema informàtic.

```markdown
# LES INTERRUPCIONS
El Linux en arrencar traçarà totes les interrupcions a dispositius disponibles en directoris dins de /proc/irq. Podem llistar-les amb ls /proc/irq.
Podem veure'n informació específica dins /proc/interrupts fent un cat d'aquest fitxer. Hi veurem la distribució de les peticions (interrupcions) de cada dispositiu en cada nucli/thread del processador.

En sistemes operatius Fedora/RedHat/CentOS existeix un servei anomenat **tuned** que permet seleccionar quina finalitat tindrà el nostre sistema operatiu per tal d'optimitzar-ne la gestió d'aquests recursos.

Amb l'ordre **tuned-adm active** podrem veure quin perfil està actualment en ús. I amb **tuned-adm list** podrem veure els perfils que es poden seleccionar.
```

### 1.5.3 Recursos dels sistemes operatius. Concurrència, comunicació, sincronització i interbloqueig de processos

**Concurrència**

Cal que el sistema operatiu eviti que dos processos accedeixin al mateix moment al mateix recurs, ja que això podria comportar un error greu al sistema. Per tal que s'executin de manera paral·lela diversos processos en un únic processador caldrà que hi hagi una certa sincronització de la que se n'encarregarà el sistema operatiu.

**Comunicació**

Els processos necessitaran tot sovint compartir dades i per això el sistema operatiu ha d'establir mecanismes de comunicació entre aquests processos.

**Interbloqueig** (deadlock)

En l'execució d'un procés pot ser necessari que un altre procés proporcioni dades. Això no serà un problema si aquest segon procés en algun moment proporciona aquestes dades. El problema serà si s'han programat aquests dos processos de manera que en alguna situació es pot donar que ambdós processos es quedin esperant a que l'altre procés proporcioni dades. En aquest moment estarem en una situació d'interbloqueig que cal evitar ja que faria que mai sortissin d'aquest estat.

```markdown
# ELS PROCESSOS
Fem una ullada als processos que corren al nostre sistema operatiu Linux Existeix la comanda **ps** que ens proporcionarà informació dels processos. Sempre podem veure l'ajuda d'una comanda amb el manual: **man ps**
Amb **ps auxf --forest** veurem un arbre dels processos. És a dir, que un procés pare pot tenir processos fills.

Una altra manera de veure els processos és amb la utilitat **top**. Aquí, a part d'informació d'ús dels recursos, també ens mostrarà un llistat dels processos. Fixeu-vos que hi ha un procés pare (inicial) que té l'identificador (PID) 1 i que s'anomena init.
```

## 1.6 - Arquitectura del sistema operatiu

**Nucli**

- Manipulació d’interrupcions.
-  Creació i destrucció de processos.
-  Canvi d’estats de processos.
-  Distribució (*dispatcher*).
-  Suspensió i represa de processos.
-  Sincronització de processos.
-  Comunicació entre processos.
-  Manipulació de blocs de control de procés.
-  Suport d’activitats d’E/S.
-  Suport de l’assignació i des-assignació d’emmagatzematge.
-  Suport del sistema d’arxius.
-  Suport de mecanismes de crida/ retorn al procediment.
-  Suport de certes funcions estadístiques del sistema.

**Utilitats de baix nivell**

- Compiladors: Tradueixen un fitxer de codi font escrit en algun llenguatge d'alt nivell al llenguatge assembler de la màquina.
- Linkadors: Enllacen diferents fitxers binaris (compilats) per crear un únic fitxer, l'executable del programa.
- Depuradors: Són aplicatius que s'encarreguen d'executar pas a pas un programa i ens alerten de problemàtiques, errors i valors de les variables per tal d'analitzar-ne i solucionar les problemàtiques.

**Interfície d'usuari**

- CLI
- GUI

A la interfície d'usuari hi haurà l'**intèrpret d'ordres** que proporciona l'entorn de treball de l'usuari

## 1.7 - Evolució històrica. Sistemes operatius actuals
[Revolution OS](https://www.youtube.com/watch?v=LgzK3fnJ3iI)

[The Code: Story of Linux documentary](https://www.youtube.com/watch?v=FMTpIi6Dr30)



## 1.8 - Classificació dels sistemes operatius

**Segons la utilització de recursos:**

- Monoprogramats: Només poden executar un programa. Són específics.
- Multiprogramats o multitasca

**Segons la interactivitat:**

- Sistemes de processament per lots: El treball a realitzar es divideix en lots executats a la plegada.
- Sistemes de temps compartit: Cada programa té un temps limitat d'accés a la CPU. Els processos quedaran 'aturats' fins al següent interval.
- Sistemes en temps real:: Són sistemes multiprogramats i interactius. Controlen dispositius que necessiten una resposta immediata

**Segons el nombre d'usuaris:**

- Monousuari 
- Multiusuari

**Segons tipus d'aplicació:**

- Sistemes de propòsit general
- Sistemes de propòsit especial

**Segons el nombre de processadors:**

- Monoprocessador
- Multiprocessador

**Segons la distribució de les tasques del sistema:**

- Distribuïts
- Centralitzats

## 1.9 - Gestió de processos. Estats dels processos. Prioritat i planificació

### 1.9.1 Els processos
Cal un algorisme de planificació per tal de poder executar les seqüències d'accions derivades de l'execució de les instruccions.

### 1.9.2 Operacions amb els processos

1. Creació de processos: 
   1. De manera jeràrquica: Cada nou procés serà fill del procés creador. El procés creador s'anomena procés pare.
   2. De manera no jeràrquica: Cada procés creat per un altre serà executat independentment del creador en un entorn completament separat.
2. Destrucció de processos: S'alliberen els recursos que estava fent servir. Els seus processos fills probablement també seran destruïts.
3. Canvi de prioritats de processos
4. Adormir o bloquejar processos i despertar-los: Es sol fer durant un temps determinat
5. Suspendre/Continuar processos: Només es fa en moments crítics (mal funcionament, elevada càrrega, ...)

### 1.9.3 Tipus de processos

- Reutilitzables: Els programes d'usuari són reutilitzables ja que modifiquen les dades que fan servir en cada execució.
- Re-entrants: Aquests processos no tenen dades associades, fan servir registres interns que no es poden modificar durant la seva utilització. Els programes compartits per usuaris a la vegada serien reentrants.

### 1.9.4 Estats dels processos

- Actius: Es troben competint per l'ús del processador.
  - Execució
  - Preparat: Es troba en cua.
  - Bloquejat: A l'espera de l'alliberament d'algun recurs.
- Inactius: En el moment actual no competeixen per l'ús de processador.
  - Suspès bloquejat: A l'espera de que passi un esdeveniment.
  - Suspès preparat: No hi ha una causa clara.

### 1.9.5 Planificació del processador

Un planificador ha de tenir com a objectius:

1. Equitat. Garantir que cada procés obtingui la proporció justa de la CPU.
2. Eficàcia. Mantenir ocupada la CPU el cent per cent del temps.
3. Temps de resposta. Minimitzar el temps de resposta per als usuaris interactius.
4. Rendiment. Maximitzar el nombre de tasques processades per hora.

Hi ha tres tipus de planificació que es poden fer:

1. A llarg termini: Decideix el proper treball a executar i s'encarrega de carregar els processos.
2. A mig termini: Decideix quins processos es troben en memòria o es mouen a 'swap'/disc. 
3. A curt termini: Decideix quan un procés que és a la cua de processos a executar té accés al processador.

El planificador ha de tenir en compte:
1. Temps de resposta
2. Temps de servei
3. Temps d'execució
4. Temps del processador
5. Temps d'espera
6. Eficiència
7. Rendiment

Aquests són algorismes utilitzats per planificar a curt termini:

1. L’algorisme d’ordre d’arribada.
El primer procés que fa petició serà el primer procés executat. Tots es posen a la cua per ordre d'arribada. Hi ha un temps fixat d'execució de cada procés.
2. L’algorisme de repartiment de temps.
El primer procés que fa petició serà el primer procés executat. Els processos tornaran a la cua una vegada acabat el seu temps d'assignació o bé si el procés s'atura esperant una operació d'entrada/sortida (espera dades) 
3. L’algorisme del procés següent.
Primer s'executaran els processos que requereixen menys temps per finalitzar. Aquí cal que el sistema tingui informació d'anteriors execucions per estimar el temps.
4. L’algorisme del procés de temps restant més curt.
És igual que l'anterior amb la diferència que si arriba un procés més curt que el que actualment s'està executant, aquest passa a la cua per tal d'executar el nou procés primer.

### 1.9.6 Algorismes de prioritats

Pels processos a les cues es pot assignar prioritats. Aquestes prioritats poden ser assignades pel sistema (internet) o bé per l'usuari (externes)

## 1.10 - Gestió de la memòria
La irrupció dels sistemes multiprogramats fa necessari prendre decisions sobre aspectes tan diversos com quant espai es dedica a cada procés, de quina manera se li assigna, en quin lloc s’ubica, durant quant de temps roman en memòria, què passa si no hi ha prou espai o com es protegeix davant accessos incorrectes. Tots aquests factors seran valorats de primer per tècniques d’assignació contigua i per mètodes d’assignació no contigua (paginació, segmentació i segmentació paginada). 

### Nivells de memòria:
- Els registres: Són els més ràpids ja que són interns de la cpu però de molt poca capacitat. (KB)
- La memòria cau: És una memòria petita i molt ràpida que es troba prop del processador (habitualment dins del mateix xip) (MB)
- La memòria principal: És la memòria RAM. Una mica més lenta que les anteriors però de capacitat molt més gran. (GB)
- La memòria secundària: Ès la memòria SWAP que es troba en disc i que es fa servir quan s'omple la RAM. Molt més lenta (GB)

### Tèniques per a organitzar i gestionar la memòria:
#### Protecció de la memòria
Els diferents processos que s'estan executant i les seves dades han d'estar en particions independents. Cada partició és un espai reservar pel procediment i les seves dades.
- Particions contigües de grandària fixa: El sistema operatiu crea les particions en arrencar d'una mida fixa i invariable fins que s'apagui.
- Particions contigües de grandària variable: Els sistema operatiu pot anar modificant la grandària segons la demanda dels processos.

#### Memòria virtual
Els sistemes operatius actuals fan ús d'aquesta tècnica que es basa en que els programes estan formats per moltes rutines que no s'usen sempre i poden no estar en memòria principal sempre.
Per tant podem estar executant programes que en teoria ocuparien més que la memòria física disponible.

Això facilita que els programadors no s'hagin de preocupar per l'espai físic de memòria disponible. El sistema operatiu intentarà proporcionar un espai virtual molt més gran del real.

En aquests sistemes s'utilitza la tècnica de **paginació**. Això permet que els programes es segmentin virtualment en pàgines (espais de memòria de mida fixa) i que físicament es troben en algun **frame** que és un espai de mida idèntic a la memòria física.
El sistema operatiu pot d'aquesta manera tenir més pàgines (virtual) que frames (real) i desar cada pàgina a la memòria principal o secundària segons la càrrega del sistema i l'ús de la pàgina que en faci el procés.

## 1.11 - Gestió d'entrada/sortida

Les principals funcions són enviar ordres als dispositius, detectar les interrupcions, controlar els errors i proporcionar una interfície entre els dispositius i la resta del sistema

### Adaptador/Controlador
Hi ha dispositius que per a comunicar-se amb el processador ho fan a través d'un controlador específic (intermediari). Per exemple controladora SATA.

### Intercanvi de dades
Per tal d'enviar i rebre dades des dels dispositius es fan servir registres (espais petits de memòria al dispositiu/controlador) o bé directament es traça en espai de memòria (tarja gràfica)






[^Referències]: http://ioc.xtec.cat/materials/FP/Materials/2201_SMX/SMX_2201_M02/web/html/index.html



